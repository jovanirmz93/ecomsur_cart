### Install Dependencies

1. Node version 14.16.0
2. Angular Cli

### Run Proyect 
 1. Antes de correr el proyecto se debe ejecutar el comando npm install, de esta manera se instalaran las dependencias necesarias.
 2. para levantar el proyecto se debe ejecutar el comando ng serve -o

 # Resolution of proyect

 Lo que intente realizar para poder agregar al carrito sin un backend fue filtrar el archivo Json para de esa manera acceder al catalogo y poder extraer la informacion y de esta manera al momento de hacer click en el boton de agragar al carrito se añadiera al localstorage y con otra funcion lo que se pretendia realizar era tomar el elemento que estaba en localstorage y ponerlo en la cesta del carrito y posteriormente sacar el price con un find y sumar las cantidades para agregarlo en el campo de total.

# ShoppingCart

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
