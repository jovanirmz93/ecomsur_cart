export interface IProduct {
  id?: number,
  imageURL?: string,
  name?: string,
  type?: string,
  price?: number,
  currency?: string
}

export interface IDataProduct {
  catalog: IProduct
}