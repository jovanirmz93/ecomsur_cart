import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IDataProduct, IProduct } from '../interfaces/product.interface';

@Injectable({
  providedIn: 'root'
})
export class InfoProductService {

  constructor(
    private http: HttpClient
  ) {}

  getProducts() {
    return this.http.get<IProduct[]>('../../assets/mock.json');
  }
}
