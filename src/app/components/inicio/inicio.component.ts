import { Component, OnInit } from '@angular/core';
import { IProduct } from 'src/app/interfaces/product.interface';
import { InfoProductService } from 'src/app/services/info-product.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {

  public prod: IProduct[] = [];
  public addShop: IProduct[] = [];

  constructor(
    private products: InfoProductService
    ) { 
      this.product();
      this.getProduct();
    }

  ngOnInit(): void {
  }


  product() {
    this.products.getProducts().subscribe((d: any) => {
      const catalog = this.prod = d.catalog;
      localStorage.setItem('items', JSON.stringify(catalog));
    });
  }

  getProduct() {
    this.addShop = JSON.parse(localStorage.getItem('items'));
  }

  buy() {
    console.log('agragar al carrito');
    localStorage.setItem('item', JSON.stringify(this.addShop));
  }
}
